﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace All_In_Tools
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {

        }

        private void toolStripStatusLabel1_Click(object sender, EventArgs e)
        {

        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // on démarre sur les credit
            tabControl1.SelectedIndex = 2; 
            // on cache les onglet de debug
            Size =  new System.Drawing.Size(883, 588);  
        }

        private void treeView1_AfterSelect_1(object sender, TreeViewEventArgs e)
        {
            TreeNode node = treeView1.SelectedNode;
            // suivant ou on clique dans la liste on change la page
            if(node.Name == "md5")
            {
                tabControl1.SelectedIndex = 1;
                textsalt.Text = "kikugalanet";
            }
            else if (node.Name == "buffpang")
            {
                // On met L'onglet 0
                tabControl1.SelectedIndex = 0;

                // on Génere les données de la pages
                retourcodebuff.Text = "		AddMenu( MMI_NPC_BUFF );\r\n";
                comboBox1.Items.Clear();
                comboBox1.Items.Add("Patience");
                comboBox1.Items.Add("Félinité");
                comboBox1.Items.Add("Métabolisme");
                comboBox1.Items.Add("Vélocitée");

                // On desactive l'écriture
                comboBox1.DropDownStyle = ComboBoxStyle.DropDownList;

                // Par Défaut
                comboBox1.SelectedIndex = comboBox1.FindStringExact("Patience");
            }
            else if (node.Name == "credit")
            {
                tabControl1.SelectedIndex = 2;
            }
            
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string password = textsalt.Text;
            password += textpwd.Text; 
            byte[] encodedPassword = new UTF8Encoding().GetBytes(password);
            byte[] hash = ((HashAlgorithm)CryptoConfig.CreateFromName("MD5")).ComputeHash(encodedPassword);
            string encoded = BitConverter.ToString(hash)
            .Replace("-", string.Empty)
            .ToLower();
            textmd5.Text = encoded;
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://git.framasoft.org/Dave_Devils/All_in_Tools_LYD");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            retourcodebuff.Text = "		AddMenu( MMI_NPC_BUFF );\r\n";
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            string NewBuffname;

            if (textBox1.Text == "")
                textBox1.Text = "1";

            if (textBox2.Text == "")
                textBox2.Text = "230";

            if (Int32.Parse(textBox1.Text) > Int32.Parse(textBox2.Text))
                textBox2.Text = textBox1.Text;

            switch(comboBox1.SelectedIndex)
            {
                case 0: NewBuffname = "SI_ASS_HEAL_PATIENCE"; break;
                case 1: NewBuffname = "SI_ASS_CHEER_CATSREFLEX"; break;
                case 2: NewBuffname = "SI_ASS_CHEER_HEAPUP"; break;
                case 3: NewBuffname = "SI_ASS_CHEER_CATSREFLEX"; break;
                default: NewBuffname = "SI_FLO_SQU_BLESSSTEP"; break;
            }
            retourcodebuff.Text = retourcodebuff.Text + "		SetBuffSkill(" + NewBuffname + ", " + trackBar1.Value + ", " + textBox1.Text + ", " + textBox2.Text + ", " + textBox3.Text + "000);//" + comboBox1.Text + "\r\n";
        }

        private void trackBar1_ValueChanged(object sender, EventArgs e)
        {
            label13.Text = "Buff de Niveau :" + trackBar1.Value;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }


    }
}
